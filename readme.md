# X86_Iron #

### What is this repository for? ###

A rudimentary x86 OS kernel using Rust based off this tutorial 
[https://os.phil-opp.com/] for learning purposes.

There are several changes from the link as the Rust language evolves and 
other obstacles I run into that require work-arounds.

### Installing and Running ###

'make run' on the command line. 

### Pre-requisites ###

'sudo apt-get install xorriso'
'sudo apt-get install mtools'

If using an EFI system:
'sudo apt-get install grub-pc-bin
'grub-mkrescue /usr/lib/grub/i386-pc -o <isoname.iso> isofiles'

Install Rust nightly via:
'curl https://sh.rustup.rs -sSf | sh'

Install xargo, which is a cargo wrapper for cross compilation:
'cargo install xargo'

Install Rust into xargo via:
'rustup component add rust-src'
