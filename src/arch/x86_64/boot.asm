global start    ; Makes a label public
extern long_mode_start

section .text
bits 32         ; Specifies the following lines are 32 bit instrs


start:
  mov esp, stack_top

  call check_multiboot
  call check_cpuid
  call check_long_mode

  call set_up_page_tables
  call enable_paging

  lgdt [gdt64.pointer]
  jmp gdt64.code:long_mode_start

  mov dword [0xb8000], 0x2f4b2f4f
  hlt


; Check if multiboot by comparing with documented magic number
check_multiboot:
  cmp eax, 0x36d76289
  jne .no_multiboot
  ret


; Pass 0 as error code if not multiboot
.no_multiboot:
  mov al, "0"
  jmp error


; Check if CPUID is supported by attempting to flip the ID bit in the FLAGS
; register. If flippable, CPUID available.
; bit 21
; https://wiki.osdev.org/Setting_Up_Long_Mode#Detection_of_CPUID
check_cpuid:
  ; Copy flags into EAX via stack
  pushfd
  pop eax

  ; copy to ecx for comparing
  mov ecx, eax

  ; flip ID bit
  xor eax, 1 << 21

  ; copy eax to flags via stack
  push eax
  popfd

  ; restore flags from old version stored in ecx
  push ecx
  popfd

  ; compare eax and ecx. If equal then bit was not flipped.
  cmp eax, ecx
  je .no_cpuid
  ret


.no_cpuid:
  mov al, "1"
  jmp error


; Use cpuid to detect whether longmode can be used.
; long mode supported if bit 29 in edx is set after using cpuid
; https://wiki.osdev.org/Setting_Up_Long_Mode#x86_or_x86-64
check_long_mode:
  ; test if extended cpu info available
  mov eax, 0x80000000   ; implicit arg for cpuid
  cpuid                 ; get highest supported arg
  cmp eax, 0x80000001   ; needs to be at least 0x8000001
  jb .no_long_mode      ; if less, cpu too old

  ; use extended info to test if long mode is available
  mov eax, 0x80000001   ; arg for extended cpu info
  cpuid                 ; returns various feature bits in ecx and edx
  test edx, 1 << 29     ; test if LM-bit is set in D-register
  jz .no_long_mode      ; if not, there is no long mode
  ret


.no_long_mode:
  mov al, "2"
  jmp error


set_up_page_tables:
  ; map first PML4 entry to PDP table
  mov eax, pdp_table
  or eax, 0b11          ; set present + writeable bits
  mov [pml4_table], eax ; move to pml4 1st 4 bytes

  ; map first PDP entry to PD table
  mov eax, pd_table
  or eax, 0b11          ; set present + writeable bits
  mov [pdp_table], eax

  ; map each PD entry to a huge 2MB page
  mov ecx, 0

.map_pd_table:
  ; loop
  ; map ecx-th PD entry to a huge page that starts at address 2MB * ecx
  mov eax, 0x200000   ; 2MB
  mul ecx             ; start address of ecx-th page
  or eax, 0b10000011  ; set present + writeable + huge bits
  mov [pd_table + ecx * 8], eax   ; map ecx-th entry
  
  inc ecx
  cmp ecx, 512        ; if counter == 512, whole PD table mapped
  jne .map_pd_table

  ret


enable_paging:
  ; load PML4 address to CR3 register
  mov eax, pml4_table
  mov cr3, eax

  ; enable PAE-flag in CR4 (Physical Address Extension)
  mov eax, CR4
  or eax, 1 << 5
  mov cr4, eax

  ; set long mode bit in EFER MSR (model specific register)
  mov ecx, 0xC0000080
  rdmsr
  or eax, 1 << 8
  wrmsr

  ; enable paging in CR0 register
  mov eax, cr0
  or eax, 1 << 31
  mov cr0, eax
  
  ret


; Prints 'ERR: ' and error code to screen and hangs
; error code in ascii in al
error:
  mov dword [0xb8000], 0x4f524f45
  mov dword [0xb8004], 0x4f3a4f52
  mov dword [0xb8008], 0x4f204f20
  mov byte  [0xb800a], al
  hlt



; Stack
; Reserve 64 bytes at end
; resb -- reserve uninitialized storage space
; map the first gig with 512 2MB pages need 1 PML4, 1 PDP, 1 PD tables
section .bss
align 4096
pml4_table:
  resb 4096
pdp_table:
  resb 4096
pd_table:
  resb 4096
stack_bottom:
  resb 64
stack_top:

section .rodata
gdt64:
  dq 0  ; 0th entry

.code: equ $ - gdt64
  dq (1<<43) | (1<<44) | (1<<47) | (1<<53)
  ; Code segments have these bit flags set:
  ; descriptor type, present, executable, 64-bit

  ; Special pointer structure
.pointer:
  dw $ - gdt64 - 1
  dq gdt64