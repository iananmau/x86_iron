#![feature(lang_items)]
#![feature(panic_implementation)]
#![feature(core_intrinsics)]
#![no_std]

use core::intrinsics;
use core::panic::PanicInfo;

extern crate rlibc;

#[no_mangle]
pub extern fn rust_main() {
    let my_str = b"It is working my dude";
    let color_byte = 0x1f;

    let mut str_colored = [color_byte; 48];
    for (i, char_byte) in my_str.into_iter().enumerate() {
        str_colored[i*2] = *char_byte;
    }

    // 0xb8000 is the start of text buffer
    let buffer_ptr = (0xb8000 + 1988) as *mut _;
    unsafe { *buffer_ptr = str_colored };
    
    loop{}
}

#[lang = "eh_personality"] #[no_mangle] pub extern fn eh_personality() {}

#[panic_implementation]
#[no_mangle]
pub extern fn panic(_info: &PanicInfo) -> ! {
    unsafe { intrinsics::abort() }
}